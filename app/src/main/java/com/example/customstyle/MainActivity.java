package com.example.customstyle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.customstyle.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private TextView mTVGreeting, mTVTest;
    private AppCompatButton mButtonNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        intializeViews();
        mTVGreeting.setText("Hello, Android😊😊");
        mTVTest.setText("It's time to have lunch 😋😋😋");
        mButtonNext.setOnClickListener(view1 -> {
            Intent intent = new Intent(this, MainActivity2.class);
            startActivity(intent);
        });
    }
    private void intializeViews () {
        mTVGreeting = binding.textView;
        mTVTest = binding.TextViewtest;
        mButtonNext = binding.button;
    }
}