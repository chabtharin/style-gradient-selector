package com.example.customstyle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.view.View;

import com.example.customstyle.databinding.ActivityMain2Binding;

public class MainActivity2 extends AppCompatActivity {
private ActivityMain2Binding binding;
private AppCompatButton mBtnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        onInitialViews();
        mBtnBack.setOnClickListener(view1 -> {
            finish();
        });
    }
    private void onInitialViews () {
        mBtnBack = binding.btnBack;
    }
}